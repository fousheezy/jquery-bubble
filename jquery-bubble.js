$.fn.bubble = function(options) {
    
    var settings = $.extend({
        speed: 10
    }, options);

    return $(this).each(function() {
        
        var $this = $(this),
            $parent = $this.parent(),
            height = $parent.height(),
            width = $parent.width(),
            top = Math.floor(Math.random() * (height / 2)) + height / 4,
            left = Math.floor(Math.random() * (width / 2)) + width / 4,
            vectorX = settings.speed * (Math.random() > 0.5 ? 1 : -1),
            vectorY = settings.speed * (Math.random() > 0.5 ? 1 : -1);

        // place initialy in a random location
        $this.css({
            'top': top,
            'left': left
        }).data('vector', {
            'origX': vectorX,
            'origY': vectorY,
            'magnitude': Math.sqrt(vectorX * vectorX + vectorY * vectorY),
            'm2': vectorX * vectorX + vectorY * vectorY,
            'x': vectorX,
            'y': vectorY,
            'lastChanged': Date.now()
        });

        var move = function($e) {
            
            var offset = $e.offset(),
                width = $e.width(),
                height = $e.height(),
                vector = $e.data('vector'),
                $parent = $e.parent();

            var canChangeDirection = true;
            if (offset.left <= 0 && vector.x < 0) {
                vector.x = -1 * vector.x;
                canChangeDirection = false;
            }
            if ((offset.left + width) >= $parent.width()) {
                vector.x = -1 * vector.x;
                canChangeDirection = false;
            }
            if (offset.top <= 0 && vector.y < 0) {
                vector.y = -1 * vector.y;
                canChangeDirection = false;
            }
            if ((offset.top + height) >= $parent.height()) {
                vector.y = -1 * vector.y;
                canChangeDirection = false;
            }
            if (!canChangeDirection) {
                vector.lastChanged = Date.now();
            }
            if (canChangeDirection && (Math.random() > .7 || vector.lastChanged - Date.now() > 5000)) {
                vector.lastChanged = Date.now();
                // amount by which to change
                var delta = Math.random() * .5;
                // slightly change direction
                // change the x or the y component?
                if (Math.random() > 0.5) {
                    delta *= vector.x; // 0-30% direction change
                    if (Math.abs(vector.x + delta) > vector.magnitude || Math.random() > 0.5) {
                        delta *= -1;
                    }
                    vector.x += delta;
                    // preserve the vector magnitude and direction of y
                    var signY = (vector.y > 0) ? 1 : -1;
                    vector.y = Math.sqrt(vector.m2 - vector.x * vector.x) * signY;
                } else {
                    delta *= vector.y; // 0-30% direction change
                    if (Math.abs(vector.y + delta) > vector.magnitude || Math.random() > 0.5) {
                        delta *= -1;
                    }
                    vector.y += delta;
                    // preserve the vector magnitude
                    var signX = (vector.x > 0) ? 1 : -1;
                    vector.x = Math.sqrt(vector.m2 - vector.y * vector.y) * signX;
                }
            }
            $e.data('vector',vector);
            $e.animate({
                'top': offset.top + vector.y + 'px',
                'left': offset.left + vector.x + 'px'
            },500,'linear',function(){
                move($e);
            });
        };
        
        move($this);
    });

};
